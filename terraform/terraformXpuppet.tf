#terraform configuration to integration with puppet
provider "aws"{
    region = "us-east-1"
}

resource "aws_instance" "puppet_intance2" {

  ami           = "ami-0440d3b780d96b29d" 
  instance_type = "t2.micro"
  subnet_id     = "subnet-0e7c68724434fb76a"

  provisioner "remote-exec" {
    inline = [
      "sudo /opt/puppetlabs/bin/puppet agent --test"
    ]
  }

  connection {
    type        = "ssh"
    user        = "ubuntu" 
    private_key = file("C:\\Users\\Ajay\\Downloads\\myec2puppet.pem")  
    host        = "54.91.226.231"  
  }
}
