#simple terraform configuration to to create aws instance
provider "aws"{
    region = "us-east-1"
}

resource "aws_instance" "demo-server1" {
    ami = "ami-0440d3b780d96b29d"
    instance_type = "t2.micro"
    key_name = "myec2key"
}
